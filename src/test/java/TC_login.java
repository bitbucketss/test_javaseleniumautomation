import org.testng.annotations.Test;
import pages.Home.HomePage;
import pages.Login.LoginPage;

public class TC_login extends BaseClass {

    @Test(groups = {"login"},alwaysRun = true,priority = 1)
    public void login () throws InterruptedException {
        driver.get(BaseURL);
       Logger.info("URL is opened");
        HomePage homePage= new HomePage(driver);
        LoginPage loginPage = new LoginPage(driver);
        Logger.info("Inside the HomePage");
        homePage.SignTrip().clickSign();
        Thread.sleep(5000);
        loginPage.setUserName(userName);
        loginPage.setPassword(password);
        loginPage.submitID();
        Logger.info("SignIn Completed");

    }
    @Test(groups ={"booking"},dependsOnGroups = {"login"} ,priority = 2)
    public void bookTicket() throws InterruptedException {
        Logger.info("Selecting the values");
        HomePage homePage= new HomePage(driver);
        homePage.IframeCount();
        homePage.SwitchFrame();
        homePage.selectFrom().selectTo().selectDate().clickSearchFlights();
        Logger.info("flights clicked");
    }


}
