import io.github.bonigarcia.wdm.WebDriverManager;

import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;

import java.util.concurrent.TimeUnit;

public class BaseClass
{
    public static WebDriver driver;
    public static org.apache.log4j.Logger Logger;
    String BaseURL = "https://www.cleartrip.com/";
    String userName ="tusharsinhaa95@gmail.com";
    String password ="Password@1";

@Parameters("browser")
@BeforeClass
public void setup(String browser) {

    if(browser.equals("chrome")) {
        WebDriverManager.chromedriver().setup();
        driver=new ChromeDriver();
        driver.manage().window().maximize();
    }
    else if(browser.equals("ie")) {
        WebDriverManager.iedriver().setup();
        driver=new InternetExplorerDriver();
        driver.manage().window().maximize(); }
    else if(browser.equals("firefox")) {
        WebDriverManager.firefoxdriver().setup();
        driver=new FirefoxDriver();
        driver.manage().window().maximize();
    }

    Logger = Logger.getLogger("eTicketing");
    PropertyConfigurator.configure("Log4j.properties");
}
@AfterClass
public void tearDown()
{
    driver.quit();
}
}
