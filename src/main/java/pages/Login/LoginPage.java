package pages.Login;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginPage {

    By userName=By.xpath("//input[@name='email']");
    By password = By.xpath("//input[@title='Your account password']");
    By submit = By.xpath("//button[@id='signInButton']");
    WebDriver driver;
    //constructor
    public LoginPage(WebDriver driver)
    {
        this.driver=driver;
    }

    public void setUserName(String uName) {
        driver.switchTo().frame("modal_window");
        WebElement user = driver.findElement(userName);
        user.sendKeys(uName);
    }
    public void setPassword(String pass) {
        WebElement userpass = driver.findElement(password);
        userpass.sendKeys(pass);
    }
    public void submitID () throws InterruptedException {
        WebElement submitLogin = driver.findElement(submit);
        submitLogin.click();
        Thread.sleep(1000);
    }
}
