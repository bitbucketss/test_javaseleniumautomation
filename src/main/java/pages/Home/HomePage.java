package pages.Home;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class HomePage {
    By signIN= By.xpath("//input[@title='Sign In']");
    By SignInTrip = By.xpath("//span[text()='Your trips']");
    By flight_tab = By.xpath("//a[@title='Find flights from and to international destinations around the world']/span[@class='productIcon flights']");
    By round = By.xpath("//input[@id='RoundTrip']");
    By departOn=By.xpath("//input[@title='Depart date']");
    By selectDate=By.xpath(".//*[@id='ui-datepicker-div']/div[1]/table/tbody");
    By from = By.id("FromTag");
    By to = By.id("ToTag");
    By search = By.xpath("//input[@id='SearchBtn']");

    WebDriver driver;

    public HomePage(WebDriver driver) {
    this.driver=driver; }

    public HomePage SignTrip() {
        driver.findElement(SignInTrip).click();
        return this;
    }
    public HomePage clickSign() {
        driver.findElement(signIN).click();
        return this;
    }

    /*
     * Count number of frames
     */
    public HomePage IframeCount() {
        List<WebElement> elements = driver.findElements(By.tagName("iframe"));
        int frameNo = elements.size();
        System.out.println("Number of iframes is " + frameNo);
        return this;
    }
    /*
     * Switch Frame
     */
    public void SwitchFrame() throws InterruptedException {
        driver.switchTo().parentFrame();
        Thread.sleep(1000);
    }

    /*
     * Select From journey
     */
    public HomePage selectFrom() throws InterruptedException {
        Thread.sleep(10000);
        WebElement selectFrom = driver.findElement(from);
        selectFrom.sendKeys("cjb");
        Thread.sleep(10000);
        selectFrom.sendKeys(Keys.TAB);
        Thread.sleep(10000);
        return this;
    }
    /*
    * Select To journey
    */
    public HomePage selectTo() throws InterruptedException {
        WebElement selectTo = driver.findElement(to);
        selectTo.sendKeys("pat");
        Thread.sleep(10000);
        selectTo.sendKeys(Keys.TAB);
        Thread.sleep(10000);
        return this;
    }

    /*
    * Select date
    */
    public HomePage selectDate() throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver,10);
        WebElement element =wait.until(ExpectedConditions.visibilityOfElementLocated(departOn));
        boolean status = element.isDisplayed();
        Thread.sleep(10000);
            driver.findElement(departOn).click();
            WebElement dateWidget = driver.findElement(selectDate);
            List<WebElement> columns = dateWidget.findElements(By.tagName("a"));
            for (WebElement cell : columns) {
                if (cell.getText().equals("25")) {
                    cell.click();
                    Thread.sleep(10000);
                    System.out.println("date is selected");
                    break;

                }
            }
            return this;
        }

        /*
        * Click Search
        */
     public HomePage clickSearchFlights() throws InterruptedException {
         driver.findElement(search).click();
         Thread.sleep(10000);
         return  this;
     }


     }



